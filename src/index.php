<?php
use Contractroom\Models\User as User;
require_once("../app/bootstrap.php");

$user = new User(getenv("MYSQL_USER"));
echo("New user created: {$user}\n");

print "env is: ".getenv("MYSQL_USER")."\n";
echo "Welcome to your first docker php project";


$conn = new mysqli("cr_database", "root", getenv("MYSQL_ROOT_PASSWORD"));
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
 
$sql = "SHOW DATABASES";
$result = $conn->query($sql);
 
if ($result->num_rows > 0) {
	// output data of each row
	echo "<ul>";
	while($row = $result->fetch_array()) {
		echo "<li>".$row[0]."</li>";
	}
	echo "</ul>";
} else {
	echo " 0 results";
}
$conn->close();


